#!/usr/bin/env python3

import discord
from config import token


class MyClient(discord.Client):
    async def on_ready(self):
        print("Logged on as {0}!".format(self.user))
        self.text_channel_list = []
        for server in self.guilds:
            for channel in server.channels:
                if str(channel.type) == "text":
                    self.text_channel_list.append(channel)
                    await channel.send("Jsem zde!!!")

    async def on_message(self, message):
        print("Message from {0.author}: {0.content}".format(message))
        if "bot" in message.content.lower():
            await message.channel.send("Já jsem tady!")


client = MyClient()
client.run(token)
